﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using Desktop_Application.Functions.Utils;

namespace Desktop_Application
{
    public partial class Main : Form
    {
        #region Variables
        // MenuPanel
        private bool menuEnabled = false;
        private readonly int menuPanelOutSize = 35;

        // Move form
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        // DropShadow
        private const int CS_DropShadow = 0x0025000;
        #endregion

        #region Constructor and Load
        public Main()
        {
            InitializeComponent();
            // Cria uma região arredondada do Form1
            Region = Region.FromHrgn(FormUtils.CreateRoundRectRgn(0, 0, Width, Height, 13, 13));

        }
        private void Main_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region Move Form DLL Import
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DropShadow;
                return cp;
            }
        }
        #endregion

        #region Form Button Events

        private void loginButton_Click(object sender, EventArgs e)
        {
        }
        #endregion

        #region Top Panel Events
        private void Main_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion

        private void MyGymStationTopPanel1_MouseDown(object sender, MouseEventArgs e)
        {

        }


        private void MyGymStationTopPanel1_MouseMove(object sender, MouseEventArgs e)
        {
        }

        private void LocaltimeTimer_Tick(object sender, EventArgs e)
        {
            lblTimer.Text = DateTime.Now.ToString("HH:mm:ss");
        }
    }
}
