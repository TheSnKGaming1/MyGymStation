﻿namespace Desktop_Application
{
    partial class Main
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.myGymStationLeftSideBar1 = new Desktop_Application.Views.MyGymStationLeftSideBar();
            this.userName = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.localtimeTimer = new System.Windows.Forms.Timer(this.components);
            this.myGymStationTopPanel1 = new Desktop_Application.Views.MyGymStationTopPanel();
            this.userImage = new System.Windows.Forms.PictureBox();
            this.homeScreen1 = new Desktop_Application.Views.HomeScreen();
            this.loginScreen1 = new Desktop_Application.Views.LoginScreen();
            this.registerScreen1 = new Desktop_Application.Views.RegisterScreen();
            this.LeftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userImage)).BeginInit();
            this.SuspendLayout();
            // 
            // LeftPanel
            // 
            this.LeftPanel.BackColor = System.Drawing.Color.Transparent;
            this.LeftPanel.Controls.Add(this.myGymStationLeftSideBar1);
            this.LeftPanel.Location = new System.Drawing.Point(0, 32);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(200, 645);
            this.LeftPanel.TabIndex = 321318;
            // 
            // myGymStationLeftSideBar1
            // 
            this.myGymStationLeftSideBar1.BackColor = System.Drawing.Color.Transparent;
            this.myGymStationLeftSideBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.myGymStationLeftSideBar1.Location = new System.Drawing.Point(0, 0);
            this.myGymStationLeftSideBar1.Name = "myGymStationLeftSideBar1";
            this.myGymStationLeftSideBar1.Size = new System.Drawing.Size(201, 645);
            this.myGymStationLeftSideBar1.TabIndex = 321325;
            // 
            // userName
            // 
            this.userName.AutoSize = true;
            this.userName.BackColor = System.Drawing.Color.Transparent;
            this.userName.Font = new System.Drawing.Font("Javanese Text", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userName.ForeColor = System.Drawing.Color.White;
            this.userName.Location = new System.Drawing.Point(1021, 61);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(177, 54);
            this.userName.TabIndex = 321320;
            this.userName.Text = "Eduardo Borba";
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.BackColor = System.Drawing.Color.Transparent;
            this.lblTimer.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer.ForeColor = System.Drawing.Color.White;
            this.lblTimer.Location = new System.Drawing.Point(1084, 38);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(114, 23);
            this.lblTimer.TabIndex = 321323;
            this.lblTimer.Text = "00:00:00";
            // 
            // localtimeTimer
            // 
            this.localtimeTimer.Enabled = true;
            this.localtimeTimer.Interval = 1000;
            this.localtimeTimer.Tick += new System.EventHandler(this.LocaltimeTimer_Tick);
            // 
            // myGymStationTopPanel1
            // 
            this.myGymStationTopPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.myGymStationTopPanel1.Location = new System.Drawing.Point(0, 0);
            this.myGymStationTopPanel1.Name = "myGymStationTopPanel1";
            this.myGymStationTopPanel1.Size = new System.Drawing.Size(1280, 33);
            this.myGymStationTopPanel1.TabIndex = 1;
            // 
            // userImage
            // 
            this.userImage.BackColor = System.Drawing.Color.Transparent;
            this.userImage.BackgroundImage = global::Desktop_Application.Properties.Resources.fotoxopada;
            this.userImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userImage.Location = new System.Drawing.Point(1204, 38);
            this.userImage.Name = "userImage";
            this.userImage.Size = new System.Drawing.Size(64, 64);
            this.userImage.TabIndex = 321319;
            this.userImage.TabStop = false;
            // 
            // homeScreen1
            // 
            this.homeScreen1.BackColor = System.Drawing.Color.Transparent;
            this.homeScreen1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("homeScreen1.BackgroundImage")));
            this.homeScreen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.homeScreen1.Location = new System.Drawing.Point(200, 31);
            this.homeScreen1.Name = "homeScreen1";
            this.homeScreen1.Size = new System.Drawing.Size(1079, 645);
            this.homeScreen1.TabIndex = 321324;
            this.homeScreen1.UseSelectable = true;
            // 
            // loginScreen1
            // 
            this.loginScreen1.BackColor = System.Drawing.Color.Black;
            this.loginScreen1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("loginScreen1.BackgroundImage")));
            this.loginScreen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.loginScreen1.Location = new System.Drawing.Point(200, 31);
            this.loginScreen1.Name = "loginScreen1";
            this.loginScreen1.Size = new System.Drawing.Size(1079, 645);
            this.loginScreen1.TabIndex = 321325;
            this.loginScreen1.Visible = false;
            // 
            // registerScreen1
            // 
            this.registerScreen1.BackColor = System.Drawing.Color.Black;
            this.registerScreen1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("registerScreen1.BackgroundImage")));
            this.registerScreen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.registerScreen1.Location = new System.Drawing.Point(200, 32);
            this.registerScreen1.Name = "registerScreen1";
            this.registerScreen1.Size = new System.Drawing.Size(1079, 645);
            this.registerScreen1.TabIndex = 321326;
            this.registerScreen1.Visible = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1280, 677);
            this.Controls.Add(this.myGymStationTopPanel1);
            this.Controls.Add(this.userImage);
            this.Controls.Add(this.LeftPanel);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.homeScreen1);
            this.Controls.Add(this.loginScreen1);
            this.Controls.Add(this.registerScreen1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            this.LeftPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.PictureBox userImage;
        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.Label lblTimer;
        private Views.MyGymStationTopPanel myGymStationTopPanel1;
        private Views.MyGymStationLeftSideBar myGymStationLeftSideBar1;
        private System.Windows.Forms.Timer localtimeTimer;
        private Views.HomeScreen homeScreen1;
        private Views.LoginScreen loginScreen1;
        private Views.RegisterScreen registerScreen1;
    }
}

