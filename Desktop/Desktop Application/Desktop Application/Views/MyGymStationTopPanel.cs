﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Desktop_Application.Views
{
    public partial class MyGymStationTopPanel : UserControl
    {
        #region Variables
        Form form;
        private bool mouseDown;
        const int WM_NCLBUTTONDOWN = 0xA1;
        const int HT_CAPTION = 0x2;

        #endregion

        #region Constructor
        public MyGymStationTopPanel()
        {
            InitializeComponent();
        }
        #endregion

        #region DLL Import
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        #region topPanel Events
        private void topPanel_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Rectangle gradient_rectangle = new Rectangle(0, 0, topPanel.Width, topPanel.Height);
            Brush b = new LinearGradientBrush(gradient_rectangle, Color.DarkOrchid, Color.Orange, 30f);
            graphics.FillRectangle(b, gradient_rectangle);
        }
        #endregion


        private void Minimize_button_MouseClick(object sender, MouseEventArgs e)
        {
            GetMainForm();
            form.WindowState = FormWindowState.Minimized;
        }

        private void Close_button_Click(object sender, EventArgs e)
        {
            GetMainForm();
            form.Close();
        }

        private void GetMainForm()
        {
            form = Application.OpenForms.OfType<Main>().FirstOrDefault();
        }

        private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if(form == null)GetMainForm();

            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(form.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

    }
}
