﻿namespace Desktop_Application.Views
{
    partial class MyGymStationLeftSideBar
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuPanel = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.sideBarTile = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.bc2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bc1 = new System.Windows.Forms.Label();
            this.myGymStationSocials1 = new Desktop_Application.Views.MyGymStationSocials();
            this.clickTimer = new System.Windows.Forms.Timer(this.components);
            this.timerMouseEnter = new System.Windows.Forms.Timer(this.components);
            this.timerMouseLeave = new System.Windows.Forms.Timer(this.components);
            this.btnRegister = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.pBoxHome = new System.Windows.Forms.PictureBox();
            this.pBoxLogin = new System.Windows.Forms.PictureBox();
            this.pBoxRegister = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.menuPanel)).BeginInit();
            this.menuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxRegister)).BeginInit();
            this.SuspendLayout();
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.Color.Black;
            this.menuPanel.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, System.Drawing.Color.DarkOrchid, System.Drawing.Color.Orange);
            this.menuPanel.Border3DStyle = System.Windows.Forms.Border3DStyle.Adjust;
            this.menuPanel.BorderColor = System.Drawing.Color.Transparent;
            this.menuPanel.BorderSides = System.Windows.Forms.Border3DSide.Left;
            this.menuPanel.BorderSingle = System.Windows.Forms.ButtonBorderStyle.None;
            this.menuPanel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.menuPanel.Controls.Add(this.pBoxHome);
            this.menuPanel.Controls.Add(this.pBoxRegister);
            this.menuPanel.Controls.Add(this.pBoxLogin);
            this.menuPanel.Controls.Add(this.sideBarTile);
            this.menuPanel.Controls.Add(this.label3);
            this.menuPanel.Controls.Add(this.bc2);
            this.menuPanel.Controls.Add(this.label2);
            this.menuPanel.Controls.Add(this.bc1);
            this.menuPanel.Controls.Add(this.myGymStationSocials1);
            this.menuPanel.Controls.Add(this.btnRegister);
            this.menuPanel.Controls.Add(this.btnLogin);
            this.menuPanel.Controls.Add(this.btnHome);
            this.menuPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(201, 645);
            this.menuPanel.TabIndex = 3;
            this.menuPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.UC_Paint);
            // 
            // sideBarTile
            // 
            this.sideBarTile.BackColor = System.Drawing.Color.Black;
            this.sideBarTile.Location = new System.Drawing.Point(0, 0);
            this.sideBarTile.Name = "sideBarTile";
            this.sideBarTile.Size = new System.Drawing.Size(6, 45);
            this.sideBarTile.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(9, 308);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 321331;
            this.label3.Text = "MouseOverBC:";
            this.label3.Visible = false;
            // 
            // bc2
            // 
            this.bc2.AutoSize = true;
            this.bc2.BackColor = System.Drawing.Color.Transparent;
            this.bc2.Location = new System.Drawing.Point(90, 308);
            this.bc2.Name = "bc2";
            this.bc2.Size = new System.Drawing.Size(13, 13);
            this.bc2.TabIndex = 321330;
            this.bc2.Text = "0";
            this.bc2.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(25, 283);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 321329;
            this.label2.Text = "BackColor:";
            this.label2.Visible = false;
            // 
            // bc1
            // 
            this.bc1.AutoSize = true;
            this.bc1.BackColor = System.Drawing.Color.Transparent;
            this.bc1.Location = new System.Drawing.Point(90, 283);
            this.bc1.Name = "bc1";
            this.bc1.Size = new System.Drawing.Size(13, 13);
            this.bc1.TabIndex = 321328;
            this.bc1.Text = "0";
            this.bc1.Visible = false;
            // 
            // myGymStationSocials1
            // 
            this.myGymStationSocials1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.myGymStationSocials1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.myGymStationSocials1.Location = new System.Drawing.Point(0, 595);
            this.myGymStationSocials1.Name = "myGymStationSocials1";
            this.myGymStationSocials1.Size = new System.Drawing.Size(201, 50);
            this.myGymStationSocials1.TabIndex = 0;
            // 
            // clickTimer
            // 
            this.clickTimer.Interval = 5;
            this.clickTimer.Tick += new System.EventHandler(this.ClickTimer_Tick);
            // 
            // timerMouseEnter
            // 
            this.timerMouseEnter.Interval = 10;
            this.timerMouseEnter.Tick += new System.EventHandler(this.timerMouseEnter_Tick);
            // 
            // timerMouseLeave
            // 
            this.timerMouseLeave.Interval = 10;
            this.timerMouseLeave.Tick += new System.EventHandler(this.TimerMouseLeave_Tick);
            // 
            // btnRegister
            // 
            this.btnRegister.BackColor = System.Drawing.Color.Transparent;
            this.btnRegister.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegister.FlatAppearance.BorderSize = 0;
            this.btnRegister.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnRegister.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegister.Font = new System.Drawing.Font("Segoe UI", 15.75F);
            this.btnRegister.ForeColor = System.Drawing.Color.Black;
            this.btnRegister.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRegister.Location = new System.Drawing.Point(0, 90);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnRegister.Size = new System.Drawing.Size(201, 45);
            this.btnRegister.TabIndex = 321332;
            this.btnRegister.Text = "Registro";
            this.btnRegister.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRegister.UseVisualStyleBackColor = false;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnLogin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Segoe UI", 15.75F);
            this.btnLogin.ForeColor = System.Drawing.Color.Black;
            this.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogin.Location = new System.Drawing.Point(0, 45);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(201, 45);
            this.btnLogin.TabIndex = 321327;
            this.btnLogin.Text = "Login";
            this.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogin.UseVisualStyleBackColor = false;
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.Transparent;
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Segoe UI", 15.75F);
            this.btnHome.ForeColor = System.Drawing.Color.Black;
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHome.Location = new System.Drawing.Point(0, 0);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(201, 45);
            this.btnHome.TabIndex = 321324;
            this.btnHome.Text = "Home";
            this.btnHome.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnHome.UseVisualStyleBackColor = false;
            // 
            // pBoxHome
            // 
            this.pBoxHome.BackColor = System.Drawing.Color.Transparent;
            this.pBoxHome.Image = global::Desktop_Application.Properties.Resources.go_home;
            this.pBoxHome.Location = new System.Drawing.Point(36, 9);
            this.pBoxHome.Name = "pBoxHome";
            this.pBoxHome.Size = new System.Drawing.Size(32, 32);
            this.pBoxHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBoxHome.TabIndex = 321333;
            this.pBoxHome.TabStop = false;
            // 
            // pBoxLogin
            // 
            this.pBoxLogin.BackColor = System.Drawing.Color.Transparent;
            this.pBoxLogin.Image = global::Desktop_Application.Properties.Resources.login;
            this.pBoxLogin.Location = new System.Drawing.Point(36, 52);
            this.pBoxLogin.Name = "pBoxLogin";
            this.pBoxLogin.Size = new System.Drawing.Size(32, 32);
            this.pBoxLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBoxLogin.TabIndex = 321334;
            this.pBoxLogin.TabStop = false;
            // 
            // pBoxRegister
            // 
            this.pBoxRegister.BackColor = System.Drawing.Color.Transparent;
            this.pBoxRegister.Image = global::Desktop_Application.Properties.Resources.register;
            this.pBoxRegister.Location = new System.Drawing.Point(36, 96);
            this.pBoxRegister.Name = "pBoxRegister";
            this.pBoxRegister.Size = new System.Drawing.Size(32, 32);
            this.pBoxRegister.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBoxRegister.TabIndex = 321335;
            this.pBoxRegister.TabStop = false;
            // 
            // MyGymStationLeftSideBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.menuPanel);
            this.Name = "MyGymStationLeftSideBar";
            this.Size = new System.Drawing.Size(201, 645);
            ((System.ComponentModel.ISupportInitialize)(this.menuPanel)).EndInit();
            this.menuPanel.ResumeLayout(false);
            this.menuPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxRegister)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MyGymStationSocials myGymStationSocials1;
        private Syncfusion.Windows.Forms.Tools.GradientPanel menuPanel;
        private System.Windows.Forms.Panel sideBarTile;
        private System.Windows.Forms.Timer clickTimer;
        private System.Windows.Forms.Timer timerMouseEnter;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Timer timerMouseLeave;
        private System.Windows.Forms.Label bc1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label bc2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.PictureBox pBoxRegister;
        private System.Windows.Forms.PictureBox pBoxLogin;
        private System.Windows.Forms.PictureBox pBoxHome;
    }
}
