﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desktop_Application.Views
{
    public partial class MyGymStationSocials : UserControl
    {
        public MyGymStationSocials()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(100, Color.Black);
        }

        private void ScFB_MouseHover(object sender, EventArgs e)
        {
            scFB.BackgroundImage = Properties.Resources.facebook_mgs_hover;
        }

        private void ScTW_MouseHover(object sender, EventArgs e)
        {
            scTW.BackgroundImage = Properties.Resources.twitter_mgs_hover;
        }

        private void ScIG_MouseHover(object sender, EventArgs e)
        {
            scIG.BackgroundImage = Properties.Resources.instagram_mgs_hover;
        }

        private void ScIG_MouseLeave(object sender, EventArgs e)
        {
            scIG.BackgroundImage = Properties.Resources.instagram_mgs;
        }

        private void ScTW_MouseLeave(object sender, EventArgs e)
        {
            scTW.BackgroundImage = Properties.Resources.twitter_mgs;
        }

        private void ScFB_MouseLeave(object sender, EventArgs e)
        {
            scFB.BackgroundImage = Properties.Resources.facebook_mgs;
        }
    }
}
