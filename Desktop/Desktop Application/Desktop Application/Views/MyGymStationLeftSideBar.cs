﻿using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System;
using Desktop_Application.Functions.Utils;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace Desktop_Application.Views
{
    public partial class MyGymStationLeftSideBar : UserControl
    {
        private List<Button> buttonList = new List<Button>();
        private List<PictureBox> pBoxList = new List<PictureBox>();

        private ValueTuple<Button, PictureBox> btnPBoxTuple = new ValueTuple<Button, PictureBox>();
        private ValueTuple<ValueTuple, ValueTuple, ValueTuple> vTupleList = new ValueTuple<ValueTuple, ValueTuple, ValueTuple>();

        private Button actButton, prevButton;
        private int targetBtnMov = 0, alpha = 0;
        //bool mouseLeave = false;

        public MyGymStationLeftSideBar()
        {
            InitializeComponent();

            // Define o eventHandler do painter;
            this.Paint += new PaintEventHandler(UC_Paint);

            ValueTuple.Create<Button,PictureBox>()
            foreach (var button in menuPanel.Controls.OfType<Button>())
            {
                button.MouseEnter += (sender, args) => BtnMouseEnterLeave(sender, args, "Enter");
                button.MouseLeave += (sender, args) => BtnMouseEnterLeave(sender, args);
                button.MouseDown += (sender, args) => BtnMouseEnterLeave(sender, args);
                button.MouseUp += (sender, args) => BtnMouseEnterLeave(sender, args, "Enter");
                button.Click += btnClicked;
                buttonList.Add(button);

                btnPBoxTuple.
            }

            foreach (var pBox in menuPanel.Controls.OfType<PictureBox>())
            {
                pBoxList.Add(pBox);
            }

            buttonList[2].BackColor = Color.MistyRose;
            pBoxHome.BackColor = btnHome.BackColor;

        }

        private void BtnMouseEnterLeave(object sender, EventArgs e, string eventName = "Leave")
        {
            var btn = (Button)sender;
            SetPictureBoxBackColorEnterLeave(btn, eventName);
        }

        private async void SetPictureBoxBackColorEnterLeave(Button btn, string eventName)
        {
            if (eventName.Equals("Enter"))
            {
                pBox.BackColor = btn.BackColor;
                return;
            }

            foreach (var pBox in pBoxList)
            {
                if (btn.Name.Substring(3).Contains(pBox.Name.Substring(4)))
                {
                    pBox.BackColor = btn.FlatAppearance.MouseOverBackColor;
                    return;
                }
            }
            await Task.Delay(1);
        }

        private void UC_Paint(Object sender, PaintEventArgs e)
        {
            if (this.ClientRectangle.Height == 0 || this.ClientRectangle.Width == 0) return;

            Graphics graphics = e.Graphics;
            //the rectangle, the same size as our Form
            Rectangle gradient_rectangle = new Rectangle(0, 0, Width, Height);
            //define gradient's properties
            Brush b = new LinearGradientBrush(gradient_rectangle, Color.DarkOrchid, Color.Orange, 90f);
            //apply gradient         
            graphics.FillRectangle(b, gradient_rectangle);
        }

        #region Click
        private void btnClicked(object sender, EventArgs e)
        {
            actButton = (Button)sender;

            if (prevButton == null)
            {
                prevButton = actButton;
                buttonList[2].BackColor = Color.Transparent;
                actButton.BackColor = Color.MistyRose;
                targetBtnMov = 3;
                SetScreen(actButton);
                clickTimer.Start();
            }

            var prevIndex = buttonList.IndexOf(prevButton);
            var sndIndex = buttonList.IndexOf(actButton);

            if (prevIndex > sndIndex) targetBtnMov = 3;
            else if (prevIndex < sndIndex) targetBtnMov = -3;
            else return;

            foreach (Button btn in buttonList)
            {
                btn.BackColor = Color.Transparent;
            }

            SetScreen(actButton);

            actButton.BackColor = Color.MistyRose;
            SetPictureBoxBackColorEnterLeave(actButton, "Enter");

            clickTimer.Start();
        }

        private void SetScreen(Button actButton)
        {
            var name = actButton.Name.Substring(3, actButton.Name.Length - 3);
            var Main = Application.OpenForms.OfType<Main>().First().Controls;

            //MessageBox.Show(name);

            foreach (UserControl UControl in Main.OfType<UserControl>())
            {
                //MessageBox.Show(UControl.Name);
                if (UControl.Name.Contains(name.ToLower()))
                {
                    UControl.Enabled = true;
                    UControl.Visible = true;
                }
                else
                {
                    if (UControl.Name.Contains("Screen"))
                    {
                        UControl.Enabled = false;
                        UControl.Visible = false;
                    }
                }
            }
        }

        private void ClickTimer_Tick(object sender, EventArgs e)
        {
            if (sideBarTile.Top == actButton.Top)
            {
                prevButton = actButton;
                clickTimer.Stop();
            }
            else { sideBarTile.Top += targetBtnMov; }

        }
        #endregion

        #region Fade

        private void btnMouseEnter(object sender, EventArgs e)
        {
            actButton = sender as Button;

            var A = actButton.BackColor.A;
            var A2 = actButton.FlatAppearance.MouseOverBackColor.A;


            bc1.Text = A.ToString();
            bc2.Text = A2.ToString();

            if (A >= 100 && A2 >= 100) return;
            else
                timerMouseEnter.Start();
        }

        private void timerMouseEnter_Tick(object sender, EventArgs e)
        {
            var A = actButton.BackColor.A;
            var A2 = actButton.FlatAppearance.MouseOverBackColor.A;

            bc1.Text = A.ToString();
            bc2.Text = A2.ToString();


            while (!(A >= 100 && A2 >= 100))
            {
                //if (mouseLeave) timerMouseEnter.Stop();

                bc1.Text = A.ToString();
                bc2.Text = A2.ToString();
                actButton.BackColor = Color.FromArgb(++A, 0, 0, 0);
                actButton.FlatAppearance.MouseOverBackColor = Color.FromArgb(++A, 0, 0, 0);
                return;
            }

            bc1.Text = A.ToString();
            bc2.Text = A2.ToString();

            timerMouseEnter.Stop();
        }

        private void btnMouseLeave(object sender, EventArgs e)
        {
            actButton = sender as Button;

            //mouseLeave = true;

            var A = actButton.BackColor.A;
            var A2 = actButton.FlatAppearance.MouseOverBackColor.A;

            bc1.Text = A.ToString();
            bc2.Text = A2.ToString();

            if (A >= 100 && A2 >= 100)
                timerMouseLeave.Start();
            else return;
        }

        private void TimerMouseLeave_Tick(object sender, EventArgs e)
        {
            var A = actButton.BackColor.A;
            var A2 = actButton.FlatAppearance.MouseOverBackColor.A;

            bc1.Text = A.ToString();
            bc2.Text = A2.ToString();

            while (A != 0 && A2 != 0)
            {
                bc1.Text = A.ToString();
                bc2.Text = A2.ToString();
                actButton.BackColor = Color.FromArgb(--A, 0, 0, 0);
                actButton.FlatAppearance.MouseOverBackColor = Color.FromArgb(--A, 0, 0, 0);
                return;
            }

            bc1.Text = A.ToString();
            bc2.Text = A2.ToString();

            timerMouseLeave.Stop();
        }
        #endregion
    }
}
