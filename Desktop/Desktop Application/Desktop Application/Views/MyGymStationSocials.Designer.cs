﻿namespace Desktop_Application.Views
{
    partial class MyGymStationSocials
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.scFB = new System.Windows.Forms.PictureBox();
            this.scTW = new System.Windows.Forms.PictureBox();
            this.scIG = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.scFB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scTW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scIG)).BeginInit();
            this.SuspendLayout();
            // 
            // scFB
            // 
            this.scFB.BackColor = System.Drawing.Color.Transparent;
            this.scFB.BackgroundImage = global::Desktop_Application.Properties.Resources.facebook_mgs;
            this.scFB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.scFB.Location = new System.Drawing.Point(31, 3);
            this.scFB.Name = "scFB";
            this.scFB.Size = new System.Drawing.Size(42, 42);
            this.scFB.TabIndex = 0;
            this.scFB.TabStop = false;
            this.scFB.MouseLeave += new System.EventHandler(this.ScFB_MouseLeave);
            this.scFB.MouseHover += new System.EventHandler(this.ScFB_MouseHover);
            // 
            // scTW
            // 
            this.scTW.BackColor = System.Drawing.Color.Transparent;
            this.scTW.BackgroundImage = global::Desktop_Application.Properties.Resources.twitter_mgs;
            this.scTW.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.scTW.Location = new System.Drawing.Point(79, 3);
            this.scTW.Name = "scTW";
            this.scTW.Size = new System.Drawing.Size(42, 42);
            this.scTW.TabIndex = 1;
            this.scTW.TabStop = false;
            this.scTW.MouseLeave += new System.EventHandler(this.ScTW_MouseLeave);
            this.scTW.MouseHover += new System.EventHandler(this.ScTW_MouseHover);
            // 
            // scIG
            // 
            this.scIG.BackColor = System.Drawing.Color.Transparent;
            this.scIG.BackgroundImage = global::Desktop_Application.Properties.Resources.instagram_mgs;
            this.scIG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.scIG.Location = new System.Drawing.Point(127, 3);
            this.scIG.Name = "scIG";
            this.scIG.Size = new System.Drawing.Size(42, 42);
            this.scIG.TabIndex = 2;
            this.scIG.TabStop = false;
            this.scIG.MouseLeave += new System.EventHandler(this.ScIG_MouseLeave);
            this.scIG.MouseHover += new System.EventHandler(this.ScIG_MouseHover);
            // 
            // MyGymStationSocials
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.scIG);
            this.Controls.Add(this.scTW);
            this.Controls.Add(this.scFB);
            this.Name = "MyGymStationSocials";
            this.Size = new System.Drawing.Size(200, 47);
            ((System.ComponentModel.ISupportInitialize)(this.scFB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scTW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scIG)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox scFB;
        private System.Windows.Forms.PictureBox scTW;
        private System.Windows.Forms.PictureBox scIG;
    }
}
