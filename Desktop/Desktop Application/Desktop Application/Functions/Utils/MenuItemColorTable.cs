﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desktop_Application.Functions.Utils
{
    public class MyGymStationColorTable : ProfessionalColorTable
    {
        public override Color MenuItemSelected
        {
            get { return ColorTranslator.FromHtml("#4d4d4d");}
        }

        public override Color MenuItemSelectedGradientBegin
        {
            get { return Color.FromArgb(100, 0, 0, 0); }
        }

        public override Color MenuItemSelectedGradientEnd
        {
            get { return Color.FromArgb(100, 0, 0, 0); }
        }

        public override Color MenuItemBorder
        {
            get { return Color.Transparent; }
        }
    }
}
